
function Validate() {
    var name = document.form.fName.value;
    var lname = document.form.lName.value;
    var mob = document.form.mob.value;
    var email = document.form.email.value;
    var pas = document.form.pass.value;
    var addr = document.form.address.value;
 

    if (name == "") {
        document.getElementById('username').innerHTML = "Please fill";
        return false;
    }
     if (name.length <= 2 || name.length >= 40) {
        document.getElementById('username').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(name)) {
        document.getElementById('username').innerHTML = "Only characters allowed";
    }

    if (lname == "") {
    document.getElementById('lastname').innerHTML = "Please fill";
    return false;
    }
    if (lname.length <= 2 || lname.length >= 40) {
        document.getElementById('lastname').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(lname)) {
    document.getElementById('lastname').innerHTML = "Only characters allowed";
    }
    if (pas.match("(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{8,}")) {
        return true;
    } else {
        document.getElementById('pssword').innerHTML = "Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters";
        return false;
    }
    
    if (!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")) {
        document.getElementById('emails').innerHTML = "Not a valid email";
        return false;
    }
    if (email.length == "") {
        document.getElementById('emails').innerHTML = "Email cannot be blank";
        return false;
    }
    if (isNaN(mob)) {
        document.getElementById('mobile').innerHTML = "Cannot be characters";
        return false;
    }
    if (mob.length != 10) {
        document.getElementById('mobile').innerHTML = "Should be of 10 digit";
        return false;
    }
    if (addr == "") {
        document.getElementById('add').innerHTML = "Address cant be empty";
        return false;
    }
    if (document.getElementById('rad1').checked==false && document.getElementById('rad2').checked==false) {
        document.getElementById('gend').innerHTML = "Select gender";
        return false;
    }
}