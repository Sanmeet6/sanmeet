
function Validate() {
   
    var name = document.form.uName.value;
    var lname = document.form.lName.value;
    var mob = document.form.mob.value;
    var email = document.form.email.value;
    var pas = document.form.pass.value;
    var addr = document.form.address.value;
    var gend = document.form.gridRadios.value;
    if (name == "") {
        document.getElementById('username').innerHTML = "Please fill";
        return false;
    }
    if (name.length <= 2 || name.length >= 40) {
        document.getElementById('username').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(name)) {
        document.getElementById('username').innerHTML = "Only characters allowed";
    }

    if (lname == "") {
        document.getElementById('lastname').innerHTML = "Please fill";
        return false;
    }
    if (lname.length <= 2 || lname.length >= 40) {
        document.getElementById('lastname').innerHTML = "Lenght must be between 2 and 20";
        return false;
    }
    if (!isNaN(lname)) {
        document.getElementById('lastname').innerHTML = "Only characters allowed";
    }
    if (!pas.match("(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{8,}")) {

        document.getElementById('pssword').innerHTML = "Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters";
        return false;
    }

    if (!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")) {
        document.getElementById('emails').innerHTML = "Not a valid email";
        return false;
    }
    if (email.length == "") {
        document.getElementById('emails').innerHTML = "Email cannot be blank";
        return false;
    }
    if (isNaN(mob)) {
        document.getElementById('mobile').innerHTML = "Cannot be characters";
        return false;
    }
    if (mob.length != 10) {
        document.getElementById('mobile').innerHTML = "Should be of 10 digit";
        return false;
    }
    if (addr == "") {
        document.getElementById('add').innerHTML = "Address cant be empty";
        return false;
    }
    if (document.getElementById('rad1').checked == false && document.getElementById('rad2').checked == false) {
        document.getElementById('gend').innerHTML = "Select gender";
        return false;
    }    
    this.AddRow();
    return false;
    
}

function delet(no) {
  
    document.getElementById("row" + no + "").innerHTML = "";
    
    
}
function edit(no) {
    var n = document.getElementById("namerow" + no);
    document.getElementById("input1").value = n.innerHTML;
    var ln = document.getElementById("lnamerow" + no);
    document.getElementById("input2").value = ln.innerHTML;
    var id = document.getElementById("idrow" + no);
    document.getElementById("input3").value = id.innerHTML;
    var mob = document.getElementById("mobrow" + no);
    document.getElementById("input4").value = mob.innerHTML;
    var ad= document.getElementById("addrrow" + no);
    document.getElementById("input5").value = ad.innerHTML;
    var btn = document.createElement("BUTTON");   
    btn.innerHTML = "Save";
    btn.type = "button";
    btn.id = "hell";
    document.form.appendChild(btn);
    btn.onclick = function () {
        save(no);
    }
}
function save(no) {
    var nui = document.getElementById("namerow" + no);
    var lns = document.getElementById("lnamerow" + no);
    var id = document.getElementById("idrow" + no);
    var mob = document.getElementById("mobrow" + no);
    var ad = document.getElementById("addrrow" + no);
    nui.innerHTML = document.getElementById("input1").value;;
    lns.innerHTML = document.getElementById("input2").value;;
    id.innerHTML = document.getElementById("input3").value;
    mob.innerHTML = document.getElementById("input4").value;
    ad.innerHTML = document.getElementById("input5").value;
    document.getElementById('hell').style.display = "none";
}

function AddRow() {
    debugger;
        var name = document.form.uName.value;
        var lname = document.form.lName.value;
        var mob = document.form.mob.value;
        var email = document.form.email.value;
        var pas = document.form.pass.value;
        var addr = document.form.address.value;
        var gend = document.form.gridRadios.value;
        var obj = { "uName": name, "lastname": lname, "number": mob, "id": email, "password": pas, "address": addr, "gender": gend };
        var table = document.getElementById("tab");
        var tableLength = table.rows.length;
    var row = table.insertRow(tableLength);
    table.insertRow(tableLength).outerHTML = `<tr id='row${tableLength}'><td>${tableLength / 2 + 0.5}</td><td id='namerow${tableLength}'>${obj.uName}</td><td id='lnamerow${tableLength}'>${obj.lastname}</td><td id='idrow${tableLength}'>${obj.id}</td><td id='mobrow${tableLength}'>${obj.number}</td><td>${obj.gender}</td><td id='addrrow${tableLength}'>${obj.address}</td><td><button id='but(${tableLength})'type="button" onclick='edit(${tableLength})'>EDIT</button> &nbsp <button value="delete" onclick='delet(${tableLength})'; type="button">DELETE</button></td></tr>`
        document.form.uName.value = "";
        document.form.lName.value = "";
        document.form.mob.value = "";
        document.form.email.value = "";
        document.form.pass.value = "";
        document.form.address.value = "";
        document.form.gridRadios[0].checked = false;
    document.form.gridRadios[1].checked = false;
        
    }

