﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskApp.Models
{
    public class StudentModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
    }
}
