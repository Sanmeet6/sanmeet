﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TaskApi.Models;
using TaskApp.Models;
using static System.Net.WebRequestMethods;

namespace TaskApp.Controllers
{
    public class StudentController : Controller
    {
        public IActionResult MAin() {
            return View();
        }
        public IActionResult AddStudent() {
            return View();
        }
       
       public async Task<ActionResult> AddStudents( StudentModel studentmodel)
        {
            string baseURL = "https://localhost:44330/";
            List<StudentModel> students = new List<StudentModel>();
            
            using (var client = new HttpClient())
            {          
                client.BaseAddress = new Uri(baseURL);
                var request = await client.PostAsJsonAsync("api/Student",studentmodel);
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    students = JsonConvert.DeserializeObject<List<StudentModel>>(result);
                    //return RedirectToAction("MAin");
                }
                
            }
            return View("StudentView",students);
        }


        public async Task<ActionResult> GetStudentById(StudentModel studId) {
            string baseURL = "https://localhost:44330/";
            var unique = studId.id;
            StudentModel singleData = new StudentModel();
            using (var client = new HttpClient()) { 
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync($"api/Student/{unique}");
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    singleData = JsonConvert.DeserializeObject<StudentModel>(result);
                }   
        }
            return View(singleData);
        }
        
        public async Task<ActionResult> StudentView(StudentModel studentmodel)
        {
            string baseURL = "https://localhost:44330/";
            List<StudentModel> students = new List<StudentModel>();
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Student");
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    students = JsonConvert.DeserializeObject<List<StudentModel>>(result);
                }
            }
            return View("StudentView",students);
        }


    }
}
