﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;

namespace TaskApi.Controllers {

    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        List<Student> studentList = new List<Student>()
        {
            new Student(){id=01,name="mohit",address="delhi" },
            new Student(){id=02,name="sohit",address="delhi" },
            new Student(){id=03,name="rohit",address="pune" },
            new Student(){id=04,name="Amit",address="noida" },
        };
        [HttpGet]
       
        public IEnumerable<Student> GetStudents() {
            return ((IEnumerable<Student>)studentList);
        }
        [HttpGet("{id}")]
        
        public Task<Student> GetById(int id) {
            var data = studentList.Find(x => x.id == id);
            return Task.FromResult(data);
        }
        [HttpPost]
        public IEnumerable<Student> AddStudent(Student data) {
            
            studentList.Add(data);
            return ((IEnumerable<Student>)studentList);
        }

    }
}
