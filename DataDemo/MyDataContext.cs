﻿using DataDemo.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDemo
{
    public class MyDataContext:DbContext
    {
        public MyDataContext() { }
        public MyDataContext(DbContextOptions<MyDataContext> options) : base(options)
        {


        }
        public DbSet<Student> Students { get; set; }
        public DbSet<EmailAddress> emailAddresses { get; set; }
    }
}
