﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyExample.Services
{
    public class ServiceType : IServiceType
    {
        string guid;
        public ServiceType()
        {
            this.guid = Guid.NewGuid().ToString();
        }

        public string GetGuid()
        {
            return this.guid;
        }
    }
}
