﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyExample.Services
{
    public interface IServiceType
    {
        string GetGuid();
    }
}
