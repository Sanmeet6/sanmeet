﻿using DependencyExample.Models;
using DependencyExample.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyExample.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IServiceType _serv;
        private IServiceType _serv1;
        public HomeController(ILogger<HomeController> logger, IServiceType service, IServiceType service1)
        {
            _logger = logger;
            _serv = service;
            _serv1 = service1;
        }
        [Route("")]

        public IActionResult Index()
        {
            ViewBag.guid = _serv.GetGuid();
            ViewBag.guid1 = _serv1.GetGuid();
            return View();
        }
        [Route("privacy-us")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
