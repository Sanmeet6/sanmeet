﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Friday
{
    public class House {
        public int area { get; set; }
        public House(int ar){
            area = ar;
        }
        public Door door;
        
        public void ShowData() {
            Console.WriteLine("I am a house, my area is : " + area);
        }
        public Door GetDoor() {
            return door;
        }
        public class Door {
            public string color { get; set; }
            public Door(string col) {
                color = col;
            }
            public void ShowData() {
                Console.WriteLine("I am a color, my color is : " + color);
            }
        }
    }

    public class SmallApartment:House {
        public SmallApartment(int area) : base(area) { }

    }
    public class Pers {
        public string name;
        public House house;
            public Pers(string name)
            {
                this.name = name;
            }
        public void ShowData() {
            Console.WriteLine(name);
            Console.WriteLine("House data is : ");
            house.ShowData();
            house.GetDoor().ShowData();
        }
    }
    

    class Class1
    {
        public static void Main(string[] args) {
            SmallApartment apartment = new SmallApartment();
            Pers person = new Pers("Jhon");
            apartment.door = new House.Door("black");
            person.house = apartment;
            person.ShowData();
        }
    }

}

