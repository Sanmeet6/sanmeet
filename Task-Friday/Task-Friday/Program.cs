﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Friday
{

    class Person {
        public int age;
        public string name;
        public void SetAge(int n)
        {
            age = n;
        }
        public Person(string n) {
            name = n;
        }
        public void Greet() {
            Console.WriteLine(name+ "Says hello");
        }
    }
    class Student : Person {
        public Student(string name) : base(name) { 
        
        }
        public void GoToClasses() {
            Console.WriteLine("Iam going to class");
        }
        public void ShowAge() {
            Console.WriteLine("My age is" +age );
        }
        public void Greet()
        {
            Console.WriteLine(name + "Says hello");
        }


    }
    class Teacher : Person {
        public Teacher(string name) : base(name) { }
        public void ShowAge()
        {
            Console.WriteLine("My age is" + age);
        }
        private string subject;
        public void Explain() {
            Console.WriteLine("Explaination begins");
        }
        public void Greet()
        {
            Console.WriteLine(name + "Says hello");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Morris");
            p.Greet();
            Student s = new Student("Lion");
            s.SetAge(21);
            s.Greet();
            s.ShowAge();
            Teacher t = new Teacher("Ram");
            t.SetAge(30);
            t.Greet();
            t.Explain();
            Console.ReadKey();
        }
    }
}
