﻿using CalculatorApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorApp.Controllers
{
    public class CalculatorController : Controller
    {
        private readonly ILogger<CalculatorController> _logger;

        public CalculatorController(ILogger<CalculatorController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            TempData["Key"] = "data from index method";
            return View("Calculator",new CalculateModel());
        }
        [HttpPost]
        public IActionResult Addition(CalculateModel calc)
        {
            calc.Result = calc.FirstNumber + calc.SecondNumber;
            return View("Calculator",calc);
        }
        public IActionResult Subtract(CalculateModel calc)
        {
            calc.Result = calc.FirstNumber - calc.SecondNumber;
            return View("Calculator", calc);

        }
        public IActionResult Mult(CalculateModel calc)
        {
            calc.Result = calc.FirstNumber * calc.SecondNumber;
            return View("Calculator", calc);
        }
        public IActionResult Divide(CalculateModel calc)
        {
            calc.Result = calc.FirstNumber / calc.SecondNumber;
            return View("Calculator", calc);
        }
        public IActionResult Privacy()
        {
            ViewBag.Key = TempData["key"];
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
