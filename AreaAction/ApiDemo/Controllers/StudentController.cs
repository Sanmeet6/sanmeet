﻿using ApiDemo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDemo.Controllers
{
    [ApiController]
    [Route("Api/[controller]")]
    public class StudentController : Controller
    {
        private List<Student> students = new List<Student>()
        {
            new Student(){Name="Sanmeet",Address="Delhi",Age=21 },
            new Student(){Name="mohit",Address="UP",Age=11 },
            new Student(){Name="Jeet",Address="Noida",Age=31 },
        };
        [HttpGet]
        public IEnumerable<Student> GetStudents() {
            return ((IEnumerable<Student>)students);
        }
        [HttpPost]
        [ApiVersion("4.0")]
        public IActionResult AddStudent(Student stud) {
            students.Add(stud);
            return Ok(students);
        }
    }
}
