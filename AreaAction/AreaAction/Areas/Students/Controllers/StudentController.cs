﻿using AreaAction.Areas.Students.FIlters;
using AreaAction.Models.Students;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AreaAction.Areas.Students.Controllers
{
    [Area("Students")]
    //[ResultFilter]
    public class StudentController : Controller
    {
        [ServiceFilter(typeof(ResultFilter))]
        public IActionResult Index()
        {
            return View("StudentView", new StudentModel());
        }
        public async Task<IActionResult> StudentViewAsync(StudentModel studentmodel)
        {
            string baseURL = "https://localhost:44348/";
            List<StudentModel> students = new List<StudentModel>();
            //IEnumerable<StudentModel> students = null;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Student");
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    studentmodel =JsonConvert.DeserializeObject<StudentModel>(result);
                }
            }
            return View("StudentView", studentmodel);
        }

    }
}