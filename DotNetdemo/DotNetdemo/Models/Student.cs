﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetdemo.Models
{
    
    public class Student
    {
        
        [Required]
        public string Name { get; set; }
        [Required]
        public string School { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string Standard { get; set; }

    }
}
