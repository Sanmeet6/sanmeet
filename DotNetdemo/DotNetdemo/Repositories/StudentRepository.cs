﻿using DotNetdemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetdemo.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        public List<Student> _studentList;

        public StudentRepository()
        {
            _studentList = new List<Student>() {
            new Student() { Name="sanmeet",School="DPS",Address="delhi",Gender="male",Standard="eight" },
            new Student() { Name="mohit",Standard="ninth",School="DPS",Gender="male",Address="delhi" }
        };
        }

        public List<Student> GetStudents()
        {
            return _studentList;
        }

        public void AddStudent(Student student)
        {
            _studentList.Add(student);
        }
    }
}
