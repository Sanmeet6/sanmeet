﻿using DotNetdemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetdemo.Repositories
{
    public interface IStudentRepository
    {
         List<Student> GetStudents();
        void AddStudent(Student studdent);
    }
}
