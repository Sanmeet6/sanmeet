﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MvcApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace MvcApp.Controllers
{
    public class StudentController : Controller
    {
        private readonly ILogger<StudentController> _logger;

        public StudentController(ILogger<StudentController> logger)
        {
            _logger = logger;
        }

        public async Task<ActionResult> StudentView(StudentModel studentmodel)
        {
            string baseURL = "https://localhost:5001/";
            List<StudentModel> students = new List<StudentModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Student");
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    students = JsonConvert.DeserializeObject<List<StudentModel>>(result);
                }

            }
            return View(students);
        }

        public async Task<ActionResult> Index(StudentModel studentmodel)
        {
            string baseURL = "https://localhost:5001/";
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.PostAsJsonAsync("api/Student", studentmodel);
                if (request.IsSuccessStatusCode) {
                    var result = request.Content.ReadAsStringAsync().Result;
                    studentmodel = JsonConvert.DeserializeObject<StudentModel>(result);
                }
            }
                return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
