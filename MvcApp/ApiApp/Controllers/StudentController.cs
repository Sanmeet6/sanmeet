﻿using DataFile;
using DataFile.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private DataContext _dataContext;
        public StudentController(DataContext dataContext) {
            _dataContext = dataContext;
        }
        [HttpGet]
        public Task<IList<Student>> GetStudent() 
        {
            var data = _dataContext.students.ToList();
            return Task.FromResult((IList<Student>) data);

        }
        [HttpPost]
      
        //[ProducesResponseType(typeof(IEnumerable<StudentClass>), (int)HttpStatusCode.OK)]
        public void SaveEmployee(Student employee)
        {
            var data = _dataContext.students.Add(employee);
            _dataContext.SaveChanges();
            //return (data);
        }
    }
}
