﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{  //reference in main class
    public class Location
    {
        public string Department;
        public string City;
        public Location(string city, string dept)
        {
            
            City = city;
            Department = dept;
        }
    }
}
