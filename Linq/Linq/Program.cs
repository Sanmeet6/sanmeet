﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Employee
    {
        public string Name;
        public int Age;
        public string Department;
        
        public Employee(string name, int age, string dept) {
            Name = name;
            Age = age;
            Department = dept;
        }
        
        static void Main(string[] args)
        {
            IList<Employee> employees = new List<Employee>();
            employees.Add(new Employee("Ram", 23, "Admin"));
            employees.Add(new Employee("Mohan", 21, "Admin"));
            employees.Add(new Employee("Shyam", 28, "HR"));
            employees.Add(new Employee("Jonas", 31, "Technical"));
            IList<Location> employeesLoc = new List<Location>();
            employeesLoc.Add(new Location("Punjab",  "Admin"));
            employeesLoc.Add(new Location("Pune", "HR"));

            IList<string> collection1 = new List<string>() { "One", "Two", "Three" };
            IList<string> collection2 = new List<string>() { "Four", "five" };

            //CONCAT
            var concateResult = collection1.Concat(collection2);

            foreach (string str in concateResult)
                Console.WriteLine(str);

            //FIRST
            Console.WriteLine("1st Element is: {0}", collection2.First());
            //JOIN

            var innerJoin = employees.Join(employeesLoc, e => e.Department,
                el => el.Department, (e, el) =>new
            {
                Name = e.Name,City=el.City
                                      
            });
            foreach(var loc in innerJoin)
            {
                Console.WriteLine(loc.Name+" "+loc.City);
            }

           

            //USING QUERY SYNTAX
            var emp = from e in employees
                                  where e.Age > 21 && e.Age < 32
                                  select e;
            foreach (var emps in emp)
            {
                Console.WriteLine(emps.Name );
            }

            //USING METHOD SYNTAX
            var empList = employees.Where(e => e.Age > 21 && e.Age < 32);

            foreach (var emps in empList) {
                Console.WriteLine(emps.Name+" "+emps.Department);
            }

            //ORDERBY
            var byAge = employees.OrderBy(s => s.Age);
            foreach (var empAge in byAge)
            {
                Console.WriteLine(empAge.Name);
            }

            //CONTAINS
            foreach (var dep in employees)
            {
                bool result = dep.Department.Contains("HR");
                Console.WriteLine(result);
            }

            //AVERAGE
            var avgAge = employees.Average(a => a.Age);
            Console.WriteLine(avgAge);

            //COUNT
            var count = employees.Count(c => c.Age>23);
            Console.WriteLine(count);

            //MAX
            var max = employees.Max(m => m.Age);
            Console.WriteLine(max);

            //SUM
            var sum = employees.Sum(s => s.Age);
            Console.WriteLine(sum);

            //ALL
            bool allEmp = employees.All(a => a.Age > 18 && a.Age < 26);
            Console.WriteLine(allEmp);

            //THENBY
            var then = employees.OrderBy(t => t.Age).ThenBy(t => t.Name);
            foreach (var res in then) {
                Console.WriteLine(res.Name + " " + res.Age);
                    }


            Console.ReadLine();
        }
    }
}
