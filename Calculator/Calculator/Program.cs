﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Calculator {
        public double FirstNumber()
        {
            try {
                Console.WriteLine("Enter the first number");
                double no = Convert.ToDouble(Console.ReadLine());
                return no;
            } catch (FormatException e) {
                Console.WriteLine("Please enter a valid number");
                return FirstNumber();
            }
        }
        public double SecondNumber()
        {
            try
            {
                Console.WriteLine("Enter the second number");
                double no2 = Convert.ToDouble(Console.ReadLine());
                return no2;
            }
            catch (FormatException e)
            {
                Console.WriteLine("Please enter a valid number");
                return SecondNumber();
            }
        }
        public void calculations() {
            double num1 = FirstNumber();
            double num2 = SecondNumber();
            Console.WriteLine("What operation you want to select??");
            Console.WriteLine("1. Addition");
            Console.WriteLine("2. Subtraction");
            Console.WriteLine("3. Multiplication");
            Console.WriteLine("4. Division");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice) {

                case 1:
                    Console.WriteLine("Your answer is :");
                    Console.WriteLine(num1 + num2);
                    break;
                case 2:
                    Console.WriteLine("Your answer is :");
                    Console.WriteLine(num1 - num2);
                    break;
                case 3:
                    Console.WriteLine("Your answer is :");
                    Console.WriteLine(num1 * num2);
                    break;
                case 4:
                    Console.WriteLine("Your answer is :");
                    Console.WriteLine(num1 / num2);
                    break;
                default:
                    Console.WriteLine("Wrong choice");
                    break;

            }
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            Calculator c = new Calculator();
            c.calculations();
            Console.ReadKey();
        }
    }
}
