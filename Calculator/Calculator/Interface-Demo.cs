﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    public interface IShape {
         void Calculations();
    
    }
    class Cube : IShape
    {
        public void Side()
        {
           
                int res;
                Console.WriteLine("Enter the side");
                string side = Console.ReadLine();
                bool success = Int32.TryParse(side, out res);
                if (success)
                {
                    Console.WriteLine(success);
                }
                else {
                    Console.WriteLine("no");
                }
            
            //catch (Exception e)
            //{
            //    Console.WriteLine("Please enter a valid number");
            //    return Side();
            //}
        }
        public void Calculations()
        {
            double si = Side();
            double res = si * si * si;
            double sa = 6 * si * si;
            Console.WriteLine("Volume is :" + res);
            Console.WriteLine("Surface area is :" + sa);
        }
    }
    class Cylinder:IShape {
        public double Radius()
        {
            try
            {
                Console.WriteLine("Enter the radius");
                double side = Convert.ToDouble(Console.ReadLine());
                return side;

            }
            catch (FormatException e)
            {
                Console.WriteLine("Please enter a valid number");
                return Radius();
            }
        }
        public double Height()
        {
            try
            {
                Console.WriteLine("Enter the height");
                double hi = Convert.ToDouble(Console.ReadLine());
                return hi;

            }
            catch (FormatException e)
            {
                Console.WriteLine("Please enter a valid number");
                return Height();
            }
        }
        public void Calculations() {
            double si = Radius();
            double hi = Height();

            double res = 3.14 * si * si * hi;
            double sa = 2 * 3.14 * (si + hi);
            Console.WriteLine("Volume is :" + res);
            Console.WriteLine("Surface area is :" + sa);
        }

    }
    class Sphere : IShape {
        public double Radius()
        {
            try
            {
                Console.WriteLine("Enter the radius");
                double side = Convert.ToDouble(Console.ReadLine());
                return side;

            }
            catch (FormatException e)
            {
                Console.WriteLine("Please enter a valid number");
                return Radius();
            }
        }
        public void Calculations(){
            double r = Radius();
            double vol = (4 * 3.14 * r * r * r) / 3;
            double sa = 4 * 3.14 * r * r;
            Console.WriteLine("Volume is :" + vol);
            Console.WriteLine("Surface area is :" + sa);
        }


    }
    class Interface_Demo
    {
        public static void  Main(string[] args) {
            Cylinder c = new Cylinder();
            Cube cc = new Cube();
            Sphere s = new Sphere();
            string cond;
            do
            {
                Console.WriteLine("Which shape calculations you want?");
                Console.WriteLine("1. cube");
                Console.WriteLine("2. cylinder");
                Console.WriteLine("3. sphere");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        cc.Calculations();
                        break;
                    case 2:
                        c.Calculations();
                        break;
                    case 3:
                        s.Calculations();
                        break;
                    default:
                        Console.WriteLine("Not found, enter again");
                        break;
                }


                cond = Console.ReadLine();
            } while (cond == "y" || cond == "Y");
        }
    }
}
