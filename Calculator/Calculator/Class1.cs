﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
   abstract class Shape {
        public int Side()
        {
            int Res;
            string Sides = Console.ReadLine();
            bool success = Int32.TryParse(Sides, out Res);
            if (success)
            {
                return Res;
            }
            else
            {
                Console.WriteLine("enter again");
                return Side();
            }
        }
        public double Radius()
        {
            int Res;
            string Side = Console.ReadLine();
            bool success = Int32.TryParse(Side, out Res);
            if (success)
            {
                return Res;
            }
            else
            {
                Console.WriteLine("enter again");
                return Radius();
            }
        }
        public double Height()
        {
            int res;
            string Side = Console.ReadLine();
            bool success = Int32.TryParse(Side, out res);
            if (success)
            {
                return res;
            }
            else
            {
                Console.WriteLine("enter again");
                return Height();
            }
        }
        public abstract void Calculations();
    }
    class Cube:Shape {
        
        public override void Calculations() {
            double Si = Side();
            double Res = Si * Si * Si;
            double Sa = 6 * Si * Si;
            Console.WriteLine("Volume is :" +Res);
            Console.WriteLine("Surface area is :" + Sa);
        
        }

    }
    class Cylinder:Shape
    {
        public override void Calculations()
        {
            double Si = Radius();
            double Hi = Height();
            
            double Res = 3.14 * Si * Si * Hi;
            double Sa = 2 * 3.14*(Si + Hi);
            Console.WriteLine("Volume is :" +Res);
            Console.WriteLine("Surface area is :" + Sa);

        }
    }
    class Sphere : Shape {
        
        public override void Calculations() {

            double r = Radius();
            double vol = (4 * 3.14 * r * r * r) / 3;
            double sa = 4 * 3.14 * r * r;
            Console.WriteLine("Volume is :" + vol);
            Console.WriteLine("Surface area is :" + sa);
        }
    }

    class Class1
    {
        public static void Main(string[] args)
        {
            Cylinder c = new Cylinder();
            Cube cc = new Cube();
            Sphere s = new Sphere();
            string Cond;
            do
            {
                Console.WriteLine("Which shape calculations you want?");
                Console.WriteLine("1. cube");
                Console.WriteLine("2. cylinder");
                Console.WriteLine("3. sphere");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        cc.Calculations();
                        break;
                    case 2:
                        c.Calculations();
                        break;
                    case 3:
                        s.Calculations();
                        break;
                    default:
                        Console.WriteLine("Not found, enter again");
                        break;
                }


                Cond = Console.ReadLine();
            } while (Cond == "y" || Cond == "Y");
        }
    }
}
