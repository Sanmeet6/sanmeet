﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class MusicalComposition { 
        public string Title;
        public string Composer;
        public int Year;
       public MusicalComposition(string name, string comp, int date) {
            Title = name;
            Composer = comp;
            Year = date;
        }
        public void Display() {
            Console.WriteLine("Title : "+Title);
            Console.WriteLine("Composer : "+Composer);
            Console.WriteLine("Year : "+Year);

        }

    }
    class NationalAnthem:MusicalComposition {
        string MyNation;
        public NationalAnthem(string name, string comp, int date, string nation) : base(name, comp, date) {
            MyNation = nation;
        
        }
        public void Display() {
            Console.WriteLine("Title : "+Title);
            Console.WriteLine("Composer : " +Composer);
            Console.WriteLine("Year : " +Year);
            Console.WriteLine("Nation :"+MyNation);


        }

    }
    class Prog
    {
        public static void Main(string[] args) {
            MusicalComposition mc = new MusicalComposition("Jazz", "Rehman", 1998);
            NationalAnthem na = new NationalAnthem("Pop", "Bryan adams", 1998,"Canada");
            mc.Display();
            Console.WriteLine();
            na.Display();
            Console.ReadKey();


        }
    }
}
