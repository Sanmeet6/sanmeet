﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    static class ExtensionMethod
    {
        public  static bool CheckNumber(this int i, int num) {
            return i > num;
        }
    }
}
