﻿using System;
using System.Collections.Generic;

namespace Delegates
{

    class Person
    {
        public delegate void PersonDelegate(List<Person> people);
        public string Name;
        public int Age;
        public Person(string n, int age)
        {
            Name = n;
            Age = age;
        }
        public static void PersonData(List<Person> p)
        {
            foreach(var data in p) {
                Console.WriteLine(data.Age+" "+data.Name);
            }
            
        }

        class Persondfd
        {
            static void Main(string[] args)
            {
                List<Person> Persons = new List<Person>();
                Persons.Add(new Person("Jonas", 22));
                Persons.Add(new Person("Peter", 19));
                Persons.Add(new Person("Daniel", 20));
                Persons.Add(new Person("Yvonne", 18));
                Persons.Add(new Person("Jana", 26));

                //Using simple delegate function
                PersonDelegate pDelegate = new PersonDelegate(PersonData);
                pDelegate(Persons);
                //Using anonymous method
                Person nameToFind = Persons.Find(delegate (Person p)
                  {
                      return p.Age == 19;

                  });
                Console.WriteLine(nameToFind.Age + " " + nameToFind.Name);
                //Using Lambda expression
                Person data = Persons.Find(p => p.Age > 20);
                Console.WriteLine(data.Age + " " + data.Name);
                //EXAMPLE OF EXTENSION METHOD
                int i = 10;
                bool result = i.CheckNumber(20);
                Console.WriteLine(result);

                Console.ReadKey();
            }
        }
    }
}
